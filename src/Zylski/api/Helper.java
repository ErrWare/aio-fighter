package Zylski.api;

import java.util.List;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.wrappers.interactive.Entity;
import org.dreambot.api.wrappers.interactive.GameObject;

public class Helper {

	// singleton MethodContext
	private static MethodContext mc;

	/*
	 * This method should only be called once in onStart
	 */
	public static void setMethodContext(MethodContext mc) {
		Helper.mc = mc;
	}
	
	public static void preOpenDoors(boolean force)
	{
		MethodContext.log("Trying to open a door");

		GameObject door = mc.getGameObjects().closest("Door","Large door","Longhall door","Gate");
	
		if(door != null && door.distance(mc.getLocalPlayer())<15 && door.hasAction("Open")) {
			if(!door.isOnScreen()) {
				mc.getWalking().clickTileOnMinimap(door.getTile());
				MethodContext.sleepUntil(() -> door.isOnScreen(), 4000);
			}
				
				door.interact("Open");
				MethodContext.sleep(700);
				MethodContext.sleepUntil(() -> !mc.getLocalPlayer().isMoving(), 4000);
				
				
			
		}
	}

	public static void openDoor(Entity e) {
		List<GameObject> doors = mc.getGameObjects().all("Door");
		if (doors != null && doors.size() > 0) {
			for (GameObject object : doors) {
				if (object.hasAction("Close"))
					continue;

				Tile dTile = object.getTile();
				Tile t1 = e.getTile();
				Tile t2 = mc.getLocalPlayer().getTile();

				if (dTile.getX() <= getGreaterHelper(true, t1.getX(), t2.getX())
						&& dTile.getX() >= getGreaterHelper(false, t1.getX(), t2.getX())
						&& dTile.getY() <= getGreaterHelper(true, t1.getY(), t2.getY())
						&& dTile.getY() >= getGreaterHelper(false, t1.getY(), t2.getY())) {
					while (object.hasAction("Open")) {
						object.interact("Open");
						MethodContext.sleep(100, 500);
						MethodContext.log("Trying to open door");
						if (object.hasAction("Close"))
							break;
					}
				}
			}
		}
	}
	
	private static int getGreaterHelper(boolean greater, int t1, int t2) {
		if (greater) {
			if (t1 > t2)
				return t1;
			else
				return t2;
		} else {
			if (t1 < t2)
				return t1;
			else
				return t2;
		}
	}

	public static void handleOffScreen(Entity entity) {
		if (entity != null) {
			try {
				if(!mc.getMap().canReach(entity) && !mc.getLocalPlayer().isMoving()) {
					mc.getWalking().clickTileOnMinimap(entity.getTile());
					MethodContext.sleep(200, 500);
				}
				if (!entity.isOnScreen()) {
				mc.getCamera().rotateToEntity(entity);
				}
			} catch (Exception e) {

			}
		}
	}

}
