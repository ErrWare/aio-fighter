package Zylski.api;


import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.tabs.Tab;


public class HealthManager {

	// singleton MethodContext
	private static MethodContext mc;
	
	private static int PERCENT_TO_EAT = 50;
	private static int THRESHOLD = 0;
	private static boolean doAutoEat = true;
	private static String FOOD_NAME;
	private static int currEat;
	
	// I NEED MORE IDS!!!!!!!!!!!!!!!!
	private static Integer[] LIST_OF_FOOD = {333,329,379,373,361,385};
	
	/*
	 * This method should only be called once in onStart
	 */
	public static void setMethodContext(MethodContext mc) {
		HealthManager.mc = mc;
	}

	// GETTERS AND SETTERS
	public static int getPercentToEat() {
		return PERCENT_TO_EAT;
	}

	public static void setPercentToEat(int percent) {
		PERCENT_TO_EAT = percent;
		currEat = percent;
	}

	public static void setThreshold(int threshold) {
		THRESHOLD = threshold;
		currEat = (int) ((PERCENT_TO_EAT - THRESHOLD) + ((PERCENT_TO_EAT + THRESHOLD)  - (PERCENT_TO_EAT - THRESHOLD)) * Math.random());
		
	}
	
	public static boolean isAutoEat() {
		return doAutoEat;
	}

	public static void setAutoEat(boolean autoEat) {
		HealthManager.doAutoEat = autoEat;
	}

	public static String getFoodName() {
		return FOOD_NAME;
	}

	public static void setFoodName(String foodName) {
		FOOD_NAME = foodName;
	}

	// Do we have food
	public static boolean hasFood()
	{
		if(doAutoEat)
			return mc.getInventory().contains(LIST_OF_FOOD);
		
		return mc.getInventory().contains(FOOD_NAME);
	}
	
	// Just eat one food, usually used for looting
	public static void eatOne()
	{
		if(doAutoEat) {
		if(mc.getInventory().contains(LIST_OF_FOOD)) {
			mc.getInventory().get(LIST_OF_FOOD).interact("Eat");
			MethodContext.sleep(800, 1000);
		}
		}
		else
		{
			if(mc.getInventory().contains(FOOD_NAME)) {
				mc.getInventory().get(FOOD_NAME).interact("Eat");
				MethodContext.sleep(800, 1000);
			}
		}
	}
	
	// This is the public method to eat, make sure the Manager is set up like in OnStart
	public static void eat()
	{
		if(doAutoEat)
			autoEat();
		else
			eatByName();
			
			
	}
	
	// Uses the list above to eat
	private static void autoEat()
	{
		if(mc.getLocalPlayer().getHealthPercent() <= currEat) {
			MethodContext.log("open tab: "+mc.getTabs().getOpen().getId()); 

			// Make sure our inventory tab is the one showing
			if(!mc.getTabs().isOpen(Tab.INVENTORY)) {
				MethodContext.log("Opening invenyory");
				mc.getTabs().openWithMouse(Tab.INVENTORY);
				MethodContext.sleep(500,700);  
			}
			
			
			if(mc.getInventory().contains(LIST_OF_FOOD)) {
				mc.getInventory().get(LIST_OF_FOOD).interact("Eat");
				MethodContext.sleep(1500, 2000);
				currEat = (int) ((PERCENT_TO_EAT - THRESHOLD) + ((PERCENT_TO_EAT + THRESHOLD)  - (PERCENT_TO_EAT - THRESHOLD)) * Math.random());

			}
		}
	}
	
	// Uses a specific name to eat
	private static void eatByName()
	{
		if(mc.getLocalPlayer().getHealthPercent() <= currEat) {
			MethodContext.log("open tab: "+mc.getTabs().getOpen().getId()); 

			// Make sure our inventory tab is the one showing
			if(!mc.getTabs().isOpen(Tab.INVENTORY)) {
				MethodContext.log("Opening invenyory");
				mc.getTabs().openWithMouse(Tab.INVENTORY);
				MethodContext.sleep(500,700);  
			}
			
			if(mc.getInventory().contains(FOOD_NAME)) {
				mc.getInventory().get(FOOD_NAME).interact("Eat");
				MethodContext.sleep(1500, 2000);
				currEat = (int) ((PERCENT_TO_EAT - THRESHOLD) + ((PERCENT_TO_EAT + THRESHOLD)  - (PERCENT_TO_EAT - THRESHOLD)) * Math.random());

			}
		}
	}

	

}

