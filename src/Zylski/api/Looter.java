package Zylski.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.wrappers.items.GroundItem;

import Zylski.api.GrandExchangeApi.GELookupResult;

public class Looter {

	// Variables for auto looting
	private static ArrayList<Integer> idList = new ArrayList<Integer>();
	private static ArrayList<Integer> valueList = new ArrayList<Integer>();
	private static List<String> namesToLoot;
	private static int PRICE_TO_LOOT = 0;
	private static boolean checkDoors = false;
	private static boolean doAutoLoot = true;
	private static boolean eatForLoot = true;

	// Singleton MethodContext
	private static MethodContext mc;
	static GrandExchangeApi exchangeApi;

	/*
	 * This method should only be called once in onStart
	 */
	public static void setMethodContext(MethodContext mc) {
		Looter.mc = mc;
		Looter.exchangeApi = new GrandExchangeApi();

	}

	// GETTERS AND SETTERS
	public static void setNamesToLoot(String[] namesToLoot) { Looter.namesToLoot = Arrays.asList(namesToLoot); }

	public static void setPriceToLoot(int price) {
		Looter.PRICE_TO_LOOT = price;
	}

	public static void setDoAutoLoot(boolean doAutoLoot) {
		Looter.doAutoLoot = doAutoLoot;
	}
	
	
	// Loot and item by its name
	public static void lootbyName(String lootName) {
		GroundItem loot = mc.getGroundItems().closest(lootName);

		if (loot != null) {
			if (loot.isOnScreen()) {
				if (!mc.getLocalPlayer().isMoving()) {
					loot.interact("Take");
					MethodContext.sleep(300, 600);
				}
			} else {
				mc.getWalking().clickTileOnMinimap(loot.getTile());
				MethodContext.sleep(300, 600);

			}
		}

	}

	// Loot and item by id (THIS ONE ATTEMPTS TO USE THE CHECK FOR DOORS FEATURE)
	public static void lootbyId(int lootId) {
		GroundItem loot = mc.getGroundItems().closest(lootId);
		if (loot != null) {
			pickUp(loot, false);	//false mimics previous functionality better
		}
	}

	// This loot method also checks for doors
	public static void lootbyStringArray(String[] lootString) {
		List<String> stringList = Arrays.asList(lootString);
		mc.getGroundItems().all(gi->gi!=null && stringList.contains(gi.getName())).forEach(gi -> pickUp(gi,false));
	}


	// Will not pick up coins
	public static void autoLoot() {
		// If we are not eating for loot then our inventory can not be full
		if (!eatForLoot && !mc.getInventory().isFull())
			Looter.loot();
		
		// If we are eating for loot, we need to have food to eat or a spot in the inventory
		if (eatForLoot && (HealthManager.hasFood() || !mc.getInventory().isFull()))
			Looter.loot();
	}
	
	public static void buryAllBones()
	{
		while(mc.getInventory().contains("Big bones")) {
			mc.getInventory().get("Big bones").interact("Bury");
			MethodContext.sleep(400,600);
			MethodContext.sleepUntil(() -> !mc.getLocalPlayer().isAnimating(), 3000); 
		}
	}

	private static void pickUp(GroundItem ITEM){
		pickUp(ITEM, true);
	}
	private static void pickUp(GroundItem ITEM, boolean MAKE_ROOM){
		if (checkDoors)
			Helper.openDoor(ITEM);
        if (!mc.getLocalPlayer().isMoving()){
            if (mc.getInventory().isFull() && MAKE_ROOM)
                HealthManager.eatOne();
            if (mc.getMap().canReach(ITEM) && ITEM.isOnScreen()) {
                ITEM.interact("Take");
                MethodContext.sleep(300, 600);
            } else {
                Helper.handleOffScreen(ITEM);
            }
		}
	}
	// Sorry guys I can explain this later. 
	private static void loot() {
		List<GroundItem> items = mc.getGroundItems().all();
		for (GroundItem ITEM : items) {
			final int ID = ITEM.getID();
			final String NAME = ITEM.getName();
			if (ITEM.distance() < 10) {

				if (doAutoLoot) {
					if (!idList.contains(ID)) {

						GELookupResult lookupResult = exchangeApi.lookup(ID);

						if (lookupResult != null) {
							MethodContext.log("Price for item: " + NAME + " : " + ID
									+ " :" + lookupResult.price + " X " + ITEM.getAmount());
							idList.add(ID);
							valueList.add(lookupResult.price);

							if (lookupResult.price * ITEM.getAmount() >= PRICE_TO_LOOT) {
								pickUp(ITEM);
							}
						}
					} else {
						if (valueList.get(idList.indexOf(ID))
								* ITEM.getAmount() >= PRICE_TO_LOOT) {
							pickUp(ITEM);
						}
					}
				}
				// Not really a part of auto looting, this comes from the list.
				if (namesToLoot.contains(NAME)) {
					pickUp(ITEM);
				}
			}
		}
	}

}
