
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.imageio.ImageIO;

import org.dreambot.api.methods.container.impl.equipment.EquipmentSlot;
import org.dreambot.api.methods.skills.Skill;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.methods.tabs.Tabs;
import org.dreambot.api.methods.widget.Widget;
import org.dreambot.api.methods.widget.Widgets;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.script.listener.PaintListener;
import org.dreambot.api.wrappers.items.GroundItem;
import org.dreambot.api.wrappers.items.Item;

import Zylski.api.Fighter;
import Zylski.api.HealthManager;
import Zylski.api.Looter;
import Zylski.api.top.ZylskiScript;
import org.dreambot.api.wrappers.widgets.WidgetChild;

@ScriptManifest(author = "Wesrsmith", name = "Stand United", version = 1.2, description = "Kills Everything", category = Category.COMBAT)
public class main extends ZylskiScript implements PaintListener{

	// Global variables, limit the use of these, they do not get collected by the
	// garbage collector

	private int EQUIPPED_ARROW = -1;

	// private boolean BURY_BONES = true;
	private boolean RETRIEVE_ARROWS = false;

	private boolean BURY_BONES = true;

	// The following are set by the GUI
	public String MONSTER;
	public String LOOT_GUI;
	public String FOOD_NAME;
	public String[] MONSTERS;
	public String[] LOOT;

	private long startTime;
	private long currentTime;
	
	private int startAttack = 0;
	
	public int loot = 9000000;

	public boolean arrow = false;
	public boolean autoLoot = false;
	public boolean autoEat = true;
	public boolean autoCheckDoors = false;

	public int healthPercent = 50;
	public int threshold = 5;

	public enum CombatStyle{ATTACK, STRENGTH, DEFENCE, BALANCED}
	public CombatStyle combatStyle = CombatStyle.ATTACK;

	public boolean start = false;
	
	private final Color color1 = new Color(255, 255, 255);

    private final Font font1 = new Font("Arial", 0, 14);

    private final Image img1 = getImage("http://i902.photobucket.com/albums/ac227/mrzylski/apeatollkiller.png");


	// Random Helpers
	// **********************************************************************

	// Takes a boolean and returns a Yes or No string for logging
	private String booleanToString(boolean bool) {
		if (bool)
			return "Yes";

		return "No";
	}

	// Takes the specific string (MONSTER and LOOT_GUI) and creates an array from
	// the single string.
	private String[] parseStringToArray(String s) {
		String newArray[] = s.split("\n");
		String finalArray[] = new String[newArray.length];

		for (int i = 0; i < newArray.length; i++) {
			String name = newArray[i].trim();
			log("Fighting or Looting: " + name);
			finalArray[i] = name;

		}
		return finalArray;
	}

	// // Checks if there are doors in the area
	// private boolean checkDoors() {
	//
	// // Search all objects in range with a filter of the name door. (Could use
	// getClosest("Door")
	// // I am assuming this does less work behind the scene and is more effiecient
	// (I may be wrong)
	// List<GameObject> doors = getGameObjects().all(f ->
	// f.getName().equals("Door"));
	//
	// // If the line above found a door in the area return that there are indeed
	// doors
	// if(doors != null && doors.size() > 0)
	// return true;
	//
	// // This is known as a fall through statement, if the line above did not
	// evaluate to true, then by default it must be false
	// return false;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see Zylski.api.top.ZylskiScript#onStart()
	 */
	public void onStart() {
		super.onStart();

		// GUIs are handled weirdly in scripts, there is a warning that we never use the
		// gui object which is true but I dont like warnings
		@SuppressWarnings("unused")
		ScriptGui gui = new ScriptGui(this);

		log("Welcome to my AIO Fighter");

		// Wait for the Gui start button to be pressed
		while (!start) {
			sleep(300);
		}
		log("Combat style selected: " + combatStyle);
		log("Combat style name: " + combatStyle.name());
		// Wait until logged in.
		while (!getLocalPlayer().exists() && !getLocalPlayer().isOnScreen())
			sleep(300);

		this.startTime = System.currentTimeMillis();
		this.startAttack = getSkills().getExperience(Skill.ATTACK);
		// This tells our Health manager what base percent to eat at
		HealthManager.setPercentToEat(healthPercent);
		// This tells our Health manager plus or minus some number to eat at, making it
		// a little less robotic
		HealthManager.setThreshold(threshold);

		// Sets if we are going to use autoEat function (BETA, kinda, I just need more
		// ids)
		HealthManager.setAutoEat(autoEat);

		// Sets food name to use
		HealthManager.setFoodName(FOOD_NAME);

		// Parse GUI output to be used later
		MONSTERS = parseStringToArray(MONSTER);

		// Parse GUI output in the same way as MONSTER to be used
		LOOT = parseStringToArray(LOOT_GUI);
		// This tells our looter what specifically we should pick up
		Looter.setNamesToLoot(LOOT);

		// autoLoot is from the GUI, it tells the script if we should automatically loot
		// things at a certain value
		Looter.setDoAutoLoot(autoLoot);

		// Loot price to autoloot
		Looter.setPriceToLoot(loot);

		// Are we picking up arrows?
		RETRIEVE_ARROWS = arrow;

		log("Scanning area to get to know the place");
		Widgets w = new Widgets(this.getClient());
		Tabs t = new Tabs(this.getClient());
		t.openWithFKey(Tab.COMBAT);
		List<WidgetChild> widgets = w.getWidgetChildrenContainingText("Combat");
		for(WidgetChild wc : widgets)
			log(wc.toString()+"..."+ wc.getActions().toString());
		// If we found doors
		// if (checkDoors()) {
		// log("There are doors in the area");
		// Fighter.setCheckDoors(true);
		//
		// }

		// This is a boolean weather or not to check if there are closed doors between
		// myself and the monster I want to fight
		// This is VERY BETA, I tested it once and it worked but then moved on, I will
		// come back to this
		Fighter.setCheckDoors(autoCheckDoors);

		// Assign the global variable to the currently equipped arrow, use this as
		// default. Make option to overwrite
		Item equippedArrow = getEquipment().getItemInSlot(EquipmentSlot.ARROWS.getSlot());
		if (equippedArrow != null) {
			EQUIPPED_ARROW = equippedArrow.getID();
			log("Equipped Arrows: " + equippedArrow.getName());
			log("Picking up arrows: " + booleanToString(RETRIEVE_ARROWS));
		}

	}

	public void onExit() {
		log("Buh bye :)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.dreambot.api.script.AbstractScript#onLoop()
	 * 
	 * TIP: Construct code in such a way that "sleeps" are rarely used (Besides to
	 * prevent rapid clicking, but this can be handled by the loop sleep for the
	 * most part or short waits after clicks)
	 * 
	 * Use boolean methods constructed to return true if the method successfully
	 * executed, or false if a problem arose
	 */
	@Override
	public int onLoop() {
		
		this.currentTime = System.currentTimeMillis()-this.startTime;

		
		// If we do not have food and we are no longer in combat, gracefully quit
		if (!HealthManager.hasFood() && !getLocalPlayer().isInCombat()) {
			log("Ran out of food, logging out");
			sleep(10000, 20000);
			getTabs().logout();
			return -1;
		}

		// If we have food, GAME ON!
		if (HealthManager.hasFood()) {

			// Continually check for if arrows exists, there is gotta be a better way, ill
			// revisit this later
			GroundItem loot = getGroundItems().closest(EQUIPPED_ARROW);
			GroundItem bone = getGroundItems().closest("Big bones");

			// Check if we need to eat and eat every loop
			HealthManager.eat();

			if (BURY_BONES && getInventory().isFull() && !getLocalPlayer().isInCombat()
					&& !getLocalPlayer().isAnimating() && !getLocalPlayer().isMoving()
					&& getInventory().count("Big bones") > 5) {
				Looter.buryAllBones();
			}

			// Prioritize dismissing annoying dialogs
			if (getDialogues().canContinue())
				getDialogues().clickContinue();

			/*
			 * If arrows exists and we are not in combat, take the loot. This does not
			 * prioritize the loot over fighting, this will finish a fight we are in before
			 * trying to loot. This is not the route we should go for high value loot.
			 * 
			 * NOTE: Java 'short circuits' if the first condition is false it wont check the
			 * rest. Order the statements in a way that makes sense knowing that. In most
			 * cases a null check should be first so that the rest of the condition doesnt
			 * get null pointer. In this case Seeing if we should even loot makes more
			 * sense.
			 */
			if (RETRIEVE_ARROWS && loot != null && getLocalPlayer().distance(loot.getTile()) < 6
					&& !getLocalPlayer().isInCombat() && !getLocalPlayer().isAnimating()
					&& !getLocalPlayer().isMoving()) {
				Looter.lootbyId(EQUIPPED_ARROW);

			}
			if (BURY_BONES && !getInventory().isFull() && bone != null && getLocalPlayer().distance(bone.getTile()) < 10
					&& !getLocalPlayer().isInCombat()) {
				
				if (!getLocalPlayer().isAnimating() && !getLocalPlayer().isMoving()) {
					Looter.lootbyName("Big bones");
				}
			} else {
				// If nothing else needs to be done, fight.
				Fighter.fightByName(MONSTERS);

			}

		}

		// Checks for loot every loop, doing this prioritizes loot over fighting
		Looter.autoLoot();

		return 300;

	}
	
	private Image getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch(IOException e) {
            return null;
        }
    }

	 @Override
	 public void onPaint(Graphics g1) {
        Graphics2D g = (Graphics2D)g1;
        g.drawImage(img1, 1, 3, null);
        g.setFont(font1);
        g.setColor(color1);
        int currentAttack =  getSkills().getExperience(Skill.ATTACK) - this.startAttack; 
        int attackHour = (int)(currentAttack/(this.currentTime/1000) * 3600);
        g.drawString("EXP: " + currentAttack, 310, 390);
        g.drawString("EXP / Hr : " + attackHour, 310, 416);
        int hours = (int)((this.currentTime/1000)/3600);
        int minutes = (int)((this.currentTime/1000)/60) - (hours * 60);
        int seconds = (int)((this.currentTime/1000)) - (hours * 60) - (minutes * 60);
        g.drawString(hours+" : "+ minutes + " : "+seconds, 100, 420);
    }


	
}